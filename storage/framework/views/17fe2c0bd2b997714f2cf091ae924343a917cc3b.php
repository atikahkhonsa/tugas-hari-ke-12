<?php $__env->startSection('judul'); ?>
Halaman Index
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <h1>Media Online</h1>

    <h3>Sosial Media Developer</h3>
        <p>
            Belajar dan berbagi agar hidup menjadi lebih baik
        </p>

    <h3>Benefit Join di Media Online</h3>
        <ul>
            <li>Mendapatkan motivasi dari sesama para Developer</li>
            <li>Sharing knowledge</li>
            <li>Dibuat oleh calon web developer terbaik</li>
        </ul>

    <h3>Cara Bergabung ke Media Online</h3>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Atikah-Khonsa\Atikah\PKS Digital School\Sesi 1 - Laravel\Hari Ke-12\project-hari-ke-12\resources\views/index.blade.php ENDPATH**/ ?>