<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up!</title>
</head>
<body>
    <form action="/welcome" method="POST">
        <?php echo csrf_field(); ?>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        <p>
            <label for="">First name:</label>
            <br>
            <input type="text" name="nama-depan">
        </p>
        <p>
            <label for="">Last name:</label>
            <br>
            <input type="text" name="nama-belakang">
        </p>
        <p>
            <label for="">Gender:</label>
            <br>
                <label>
                    <input type="radio" name="jenis-kelamin" value="male">
                    Male
                </label>
                <br>
                <label>
                    <input type="radio" name="jenis-kelamin" value="female">
                    Female
                </label>
                <br>
                <label>
                    <input type="radio" name="jenis-kelamin" value="other">
                    Other
                </label>
        </p>
        <p>
            <label for="">Nationality:</label>
            <br>
            <select name="nationality" id="">
                <option value="indo">Indonesian</option>
            </select>
        </p>
        <p>
            <label for="">Language Spoken:</label>
            <br>
            <input type="checkbox" name="bahasa1">Bahasa Indonesia
            <br>
            <input type="checkbox" name="bahasa2">English
            <br>
            <input type="checkbox" name="bahasa3">Other
        </p>
        <p>
            <label for="">Bio:</label>
            <br>
            <textarea name="bio" id="" cols="30" rows="10"></textarea>
        </p>
        <a href="/berhasil">
            <button type="button">Sign Up</button>
        </a>
    </form>
</body>
</html><?php /**PATH C:\Atikah-Khonsa\Atikah\PKS Digital School\Sesi 1 - Laravel\Hari Ke-12\project-hari-ke-12\resources\views/form.blade.php ENDPATH**/ ?>