@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <p>
            <label for="">First name:</label>
            <br>
            <input type="text" name="firstname">
        </p>
        <p>
            <label for="">Last name:</label>
            <br>
            <input type="text" name="lastname">
        </p>
        <p>
            <label for="">Gender:</label>
            <br>
                <label>
                    <input type="radio" name="jenis-kelamin" value="male">
                    Male
                </label>
                <br>
                <label>
                    <input type="radio" name="jenis-kelamin" value="female">
                    Female
                </label>
                <br>
                <label>
                    <input type="radio" name="jenis-kelamin" value="other">
                    Other
                </label>
        </p>
        <p>
            <label for="">Nationality:</label>
            <br>
            <select name="nationality" id="">
                <option value="indo">Indonesian</option>
            </select>
        </p>
        <p>
            <label for="">Language Spoken:</label>
            <br>
            <input type="checkbox" name="bahasa1">Bahasa Indonesia
            <br>
            <input type="checkbox" name="bahasa2">English
            <br>
            <input type="checkbox" name="bahasa3">Other
        </p>
        <p>
            <label for="">Bio:</label>
            <br>
            <textarea name="bio" id="" cols="30" rows="10"></textarea>
        </p>
        <button type="submit">Sign Up</button>
        <a href="/welcome"></a>
    </form>
@endsection