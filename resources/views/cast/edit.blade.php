@extends('layout.master')

@section('judul')
Halaman Edit Pemeran {{$cast->nama}}
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama Pemeran</label>
      <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <textarea name="umur" class="form-control" id="" cols="30" rows="10">{{$cast->umur}}</textarea>
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Biodata</label>
        <textarea name="bio" class="form-control" id="" cols="30" rows="10">{{$cast->bio}}</textarea>
      </div>
      @error('bio')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection